const puppeteer = require('puppeteer')

module.exports = {
  name: 'status',
  description: 'gets server status of games',
  cooldown: 10,
  async execute (msg) {
    async function getStatus (url) {
      const browser = await puppeteer.launch()
      const page = await browser.newPage()
      await page.goto(url)

      const [el] = await page.$x('/html/body/div[2]/div/main/article/div[1]/div[1]/div[1]/div/div[2]/div[5]/span')
      const txt = await el.getProperty('textContent')
      const status = await txt.jsonValue()

      browser.close()
      return msg.channel.send('Cold war server status: ' + status)
    }
    getStatus('https://gamingintel.com/server-status/black-ops-cold-war/')
  }
}
