require('dotenv').config({ path: '../.env' })
const puppeteer = require('puppeteer')

module.exports = {
  name: 'coing',
  description: 'gives you a chart of crypto data',
  cooldown: 10,
  async execute (msg) {
    let coin = msg.toString().replace("!coing ", "")
    async function getStatus (url) {
        console.log("working...")
        msg.channel.send("loading the graph...")
        const browser = await puppeteer.launch({ headless: true })
        const page = await browser.newPage()
        await page.goto(url, {timeout: 0})
        await page.waitFor(5000);
        await page.setViewport({ width: 1280, height: 800 })
        await page.screenshot({path: 'finance.png', fullPage: true});
        browser.close()
        return msg.channel.send({files: ["finance.png"]})
    }
    getStatus(`https://coincheckup.com/coins/${coin}`)
    }
}
