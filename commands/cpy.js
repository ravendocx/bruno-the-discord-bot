require('dotenv').config({ path: '../.env' })

module.exports = {
  name: 'cpy',
  description: 'converts text to javascript codeblocks',
  cooldown: 0,
  async execute (msg) {
    let Codeblock = "```python\n" + msg.toString().replace("!code ", "") + "\n" + "```";
    return msg.channel.send(Codeblock);
  }
}
