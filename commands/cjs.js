require('dotenv').config({ path: '../.env' })

module.exports = {
  name: 'cjs',
  description: 'converts text to javascript codeblocks',
  cooldown: 0,
  async execute (msg) {
    let Codeblock = "```js\n" + msg.toString().replace("!code ", "") + "\n" + "```";
    return msg.channel.send(Codeblock);
  }
}
