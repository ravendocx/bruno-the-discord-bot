require('dotenv').config({ path: '../.env' })
const Discord = require('discord.js');

module.exports = {
  name: 'coin',
  description: 'gets crypto info on specific coins',
  cooldown: 10,
  async execute (msg) {
    let coin = msg.toString().replace("!coin ", "").toUpperCase()

    const rp = require('request-promise');
    const requestOptions = {
        method: 'GET',
        uri: 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest',
        qs: {
            'symbol' : `${coin}`,
            'convert' : 'EUR'
        },
        headers: {
            'X-CMC_PRO_API_KEY': process.env.COINMARKETCAP_TOKEN
        },
        json: true,
        gzip: true
        };
    
        rp(requestOptions).then(response => {
      const CoinInfo = new Discord.MessageEmbed()
        .setColor('#644ee5')
        .setTitle(coin)
        .addFields(
          { name: 'Current price', value: response.data[coin].quote.EUR.price },
          { name: 'volume 24h', value: response.data[coin].quote.EUR.volume_24h },
          { name: 'Last updated', value: response.data[coin].quote.EUR.last_updated },
        )
    
      console.log('response:', response.data[coin].quote);
      return msg.channel.send(CoinInfo);
    })
    .catch((err) => {
      console.log('error:', err.message);
      return msg.channel.send('error, please make sure that you specified a currency');
    });
  }
}
