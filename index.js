require('dotenv').config()
const { readdirSync } = require('fs')
const { join } = require('path')

// discord
const DiscordClient = require('./struct/Client')
const { Collection } = require('discord.js')
const client = new DiscordClient({ token: process.env.DISCORD_TOKEN, prefix: process.env.DISCORD_PREFIX })

// look for command files
const commandFiles = readdirSync(join(__dirname, 'commands')).filter(file => file.endsWith('.js'))
for (const file of commandFiles) {
  const command = require(join(__dirname, 'commands', `${file}`))
  client.commands.set(command.name, command)
}

client.once('ready', () => console.log(`BRUNO READY, logged in as ${client.user.tag}!`))

client.on('message', msg => {
  // check message for prefix
  if (!msg.content.startsWith(client.config.prefix) || msg.author.bot) return
  const args = msg.content.slice(client.config.prefix.length).split(/ +/)

  // command handler
  const commandName = args.shift().toLowerCase()
  const command = client.commands.get(commandName) || client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName))
  if (!command) return
  if (command.guildOnly && msg.channel.type !== 'text') return msg.reply('I can\'t execute that command inside DMs!')
  if (command.args && !args.length) {
    let reply = `You didn't provide any arguments, ${msg.author}!`
    if (command.usage) reply += `\nThe proper usage would be: \`${client.config.prefix}${command.name} ${command.usage}\``
    return msg.channel.send(reply)
  }

  // cooldown handler
  if (!client.cooldowns.has(command.name)) {
    client.cooldowns.set(command.name, new Collection())
  }
  const now = Date.now()
  const timestamps = client.cooldowns.get(command.name)
  const cooldownAmount = (command.cooldown || 3) * 1000
  if (timestamps.has(msg.author.id)) {
    const expirationTime = timestamps.get(msg.author.id) + cooldownAmount
    if (now < expirationTime) {
      const timeLeft = (expirationTime - now) / 1000
      return msg.reply(`please wait ${timeLeft.toFixed(1)} more second(s) before reusing the \`${command.name}\` command.`)
    }
  }
  timestamps.set(msg.author.id, now)
  setTimeout(() => timestamps.delete(msg.author.id), cooldownAmount)

  // try to execute command
  try {
    command.execute(msg, args)
  } catch (error) {
    console.error(error)
    msg.reply('there was an error trying to execute that command!')
  }
})

client.login(client.config.token)
